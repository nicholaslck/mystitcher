/*M///////////////////////////////////////////////////////////////////////////////////////
 //
 //  IMPORTANT: READ BEFORE DOWNLOADING, COPYING, INSTALLING OR USING.
 //
 //  By downloading, copying, installing or using the software you agree to this license.
 //  If you do not agree to this license, do not download, install,
 //  copy or use the software.
 //
 //
 //                          License Agreement
 //                For Open Source Computer Vision Library
 //
 // Copyright (C) 2000-2008, Intel Corporation, all rights reserved.
 // Copyright (C) 2009, Willow Garage Inc., all rights reserved.
 // Third party copyrights are property of their respective owners.
 //
 // Redistribution and use in source and binary forms, with or without modification,
 // are permitted provided that the following conditions are met:
 //
 //   * Redistribution's of source code must retain the above copyright notice,
 //     this list of conditions and the following disclaimer.
 //
 //   * Redistribution's in binary form must reproduce the above copyright notice,
 //     this list of conditions and the following disclaimer in the documentation
 //     and/or other materials provided with the distribution.
 //
 //   * The name of the copyright holders may not be used to endorse or promote products
 //     derived from this software without specific prior written permission.
 //
 // This software is provided by the copyright holders and contributors "as is" and
 // any express or implied warranties, including, but not limited to, the implied
 // warranties of merchantability and fitness for a particular purpose are disclaimed.
 // In no event shall the Intel Corporation or contributors be liable for any direct,
 // indirect, incidental, special, exemplary, or consequential damages
 // (including, but not limited to, procurement of substitute goods or services;
 // loss of use, data, or profits; or business interruption) however caused
 // and on any theory of liability, whether in contract, strict liability,
 // or tort (including negligence or otherwise) arising in any way out of
 // the use of this software, even if advised of the possibility of such damage.
 //
 //
 //M*/

#include "common.hpp"
#include <unistd.h>
#include <stdio.h>
#include "myStitcher.hpp"

using namespace std;
using namespace cv;
using namespace cv::detail;

#define OUTPUT_FRAME_WIDTH 1920
#define OUTPUT_FRAME_HEIGHT 1080


Mat pano_output; //global variable

int main(int argc, char* argv[]) {

#if ENABLE_LOG
	LOGLN("Main start......");
	int64 start = getTickCount();
#endif

//	VideoCapture cap1(argv[1]);
	VideoCapture cap1("../../media/m1_2.mov");
	if (!cap1.isOpened())
		return -1;

//	VideoCapture cap2(argv[2]);
	VideoCapture cap2("../../media/m2_2.mov");
	if (!cap2.isOpened())
		return -1;

	vector<Mat> images(2);

	//initialize video writer


	cap1 >> images[0];
	cap2 >> images[1];
	int i = 0;
	int status;
	status = myStitcher(images, i);
	i++;

	while (i >= 0) {
		cap1 >> images[0];
		cap2 >> images[1];

		status = myStitcher(images, i);
		if (status == 0) {

			resize(pano_output, pano_output, Size(OUTPUT_FRAME_WIDTH, OUTPUT_FRAME_HEIGHT));
			pano_output = pano_output.reshape(0, 1);
			string output((char*) pano_output.data,
					pano_output.total() * pano_output.elemSize());

			//Pipe to FFmpeg | cmd: ./mystitcher | ffmpeg -re -f rawvideo -pixel_format bgr24 -video_size 1920x1080 -framerate 2 -i - out2.avi
#if not ENABLE_LOG
			cout << output;
#else
			imshow("Debug Window", pano_output);
#endif
		} else {
#if ENABLE_LOG
			LOGLN("break now");
#endif
			break;
		}
		if (waitKey(30) == 'q') {
			break;
		}
		i++;
	}
#if ENABLE_LOG
	LOGLN("Width: " << pano_output.size().width << "	Height: " << pano_output.size().height);
	LOGLN("Total time for " << i << " frames need: "<< (getTickCount() - start) / getTickFrequency());
	LOGLN("FPS: " << (float)(i/ ((getTickCount() - start) / getTickFrequency())));
#endif
	pano_output.release();
	return status;

}
