/*
 * myStitcher.hpp
 *
 *  Created on: Feb 24, 2017
 *      Author: nicholas
 */

#ifndef MYSTITCHER_HPP_
#define MYSTITCHER_HPP_

#include "common.hpp"

int myStitcher(std::vector<cv::Mat> full_img, int input_count);

#endif /* MYSTITCHER_HPP_ */
